import HttpRequest from './http_request'

export default class AuctionAPI extends HttpRequest {
  getArticles (params) {
    return this.fetch('/article/', params)
  }
  getArticle (id) {
    return this.fetch(`/article/${id}/`)
  }
}
